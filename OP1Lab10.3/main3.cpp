#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	int n;
	char ch;
	do {
		scanf_s("%d", &n);
	} while (n < 0);
	scanf(" %c", &ch);
	printf("________________________________________\n");

	for(int i = 0; i < n; i++){
		int j = 0;
		while (j < i) { printf("  "); j++;}
		printf("%c\n",ch);
	}

	return 0;
}