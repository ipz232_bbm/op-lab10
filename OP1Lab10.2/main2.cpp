#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	
	int n, j = 1;
	scanf("%d", &n);
	while (j <= n) {
		printf("%d", j);
		for (int i = 1; i <= j; i++) if (j % i == 0) printf("+");
		printf("\n");
		j++;
	}

	return 0;
}