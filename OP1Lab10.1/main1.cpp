#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	
	double sum = 0;

	for (int i = -10; i <= 10; i++) {
		double add = 1;
		if (i == 0) continue;
		for (int j = 1; j <= 3; j++) add /= i;
		sum += add;
	}
	printf("%f", sum);

	return 0;
}